package db

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/popas90/moving-myrtle/lib/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

/*func CreateDatabase(mc config.MyrtleConfig, dbName string) {
	client, err := Connect(mc)

}*/

// Ping will check if the db connection is working.
func Ping(mc config.MyrtleConfig) (bool, error) {
	connectionString := fmt.Sprintf("mongodb://%s:%d", mc.DatabaseURL, mc.Port)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionString))
	if err != nil {
		return false, err
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return false, err
	}
	return true, nil
}

// Connect starts the DB connection
func Connect(mc config.MyrtleConfig) (*mongo.Client, error) {
	connectionString := fmt.Sprintf("mongodb://%s:%d", mc.DatabaseURL, mc.Port)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionString))

	return client, err
}
