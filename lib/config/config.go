package config

import "github.com/JeremyLoy/config"

// MyrtleConfig holds env info for the app
type MyrtleConfig struct {
	DatabaseURL string `config:"DATABASE_URL"`
	FeatureFlag bool   `config:"FEATURE_FLAG"`
	Port        int    `config:"PORT"`
}

// GetConfig returns the MyrtleConfig struct
func GetConfig(whichConfig string) MyrtleConfig {
	var c MyrtleConfig
	if whichConfig == "prod" {
		config.From("prod.env").To(&c)
	} else if whichConfig == "dev" {
		config.From("dev.env").To(&c)
	} else if whichConfig == "test" {
		config.From("test.env").To(&c)
	} else {
		config.FromEnv().To(&c)
	}
	return c
}
