package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/urfave/cli"
)

var app = cli.NewApp()

func main() {
	info()
	commands()

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func info() {
	app.Name = "myrtle"
	app.Usage = "CLI for managing the moving-myrtle app"
	app.Author = "popas90"
	app.Version = "0.1"
}

func commands() {
	app.Commands = []cli.Command{
		{
			Name:   "start",
			Usage:  "Deploys all the moving-myrtle containers and starts the app.",
			Action: startAll,
		},
		{
			Name:   "stop",
			Usage:  "Stops all moving-myrtle containers.",
			Action: stopAll,
		},
		{
			Name:   "build",
			Usage:  "Builds all containers for moving-myrtle app.",
			Action: buildAll,
			Flags: []cli.Flag{
				cli.BoolFlag{Name: "prod"},
				cli.BoolFlag{Name: "dev"},
			},
		},
		{
			Name:   "check",
			Usage:  "Health check for all components of the system.",
			Action: healthCheck,
		},
		{
			Name:  "db",
			Usage: "Commands for working with the database.",
			Subcommands: []cli.Command{
				{
					Name:   "recreate",
					Usage:  "Recreates the database.",
					Action: recreateDb,
					Flags: []cli.Flag{
						cli.BoolFlag{Name: "all"},
					},
				},
				{
					Name:  "seed",
					Usage: "Populates the database with dummy test data.",
					Action: func(c *cli.Context) {
						fmt.Println("Seeding the database...")
					},
				},
			},
		},
		{
			Name:  "test",
			Usage: "Run all tests, without test coverage.",
			Action: func(c *cli.Context) {
				fmt.Println("Running all tests (without coverage metrics)...")
			},
		},
	}
}

func startAll(c *cli.Context) {
	fmt.Println("Starting moving-myrtle...")
	cmd := exec.Command("docker-compose", "up", "-d")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Deploy of moving-myrtle failed with %s\n", err)
	}
}

func stopAll(c *cli.Context) {
	fmt.Println("Stopping moving-myrtle...")
	killContainer("myrtle-users-service")
	killContainer("myrtle-db-service")
	killContainer("myrtle-db-gui")
}

func buildAll(c *cli.Context) {
	fmt.Println("Building all containers for moving-myrtle...")
	cmd := exec.Command("docker-compose", "build")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Deploy of moving-myrtle failed with %s\n", err)
	}
}

func healthCheck(c *cli.Context) {
	/*fmt.Println("Checking all components...")
	fmt.Println("Config values:")
	fmt.Println("Pinging the database...")
	response, err := db.Ping(config.GetConfig())
	if err != nil {
		if response {
			fmt.Println("Database connection successful.")
			return
		}
	}
	fmt.Println("Database connection failed.")*/
}

func recreateDb(c *cli.Context) {
	name := "somethingsomething"
	if c.NArg() > 0 {
		name = c.Args().Get(0)
	}
	if c.Bool("all") {
		fmt.Println("will recreate all dbs")
	} else {
		fmt.Println("will recreate ", name)
	}
}

func killContainer(containerName string) {
	cmd := exec.Command("docker", "container", "kill", containerName)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Could not kill container %s, failed with %s\n", containerName, err)
	}
}
