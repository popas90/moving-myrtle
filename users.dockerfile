# Start from the golang base image
FROM golang:1.12-alpine

# Add Maintainer Info
LABEL maintainer="Bogdan Popa <bogdititupopa@gmail.com>"

# The latest alpine images don't have some tools like (`git` and `bash`).
# Adding git, bash and openssh to the image
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go mod and sum files
COPY go.mod go.sum  ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Build the Go app
RUN cd cmd/users && go build

# Expose port 8080 to the outside world
EXPOSE 8080

# Command to run the executable
CMD ["./cmd/users/users"]
